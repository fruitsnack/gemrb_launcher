# gemrb_launcher

A helper script to easily launch games supported by GemRB engine.

## Requirements

- gemrb

## Usage

Consult script's --help output for usage.

Example:  
`gemrb_launcher -x 1366 -y 768 -c ~/gemrb_sample.cfg -p
~/Games/Baldursgate -g bg1`

Note: only a few arguments are required.

## License
This script is licensed under coffeeware (fork of beerware) license.  
Included gemrb_sample.cfg is taken from GemRB project and licensed under
GPL-2.0 License.
